# playper

Playlist converter from different formats.

Rosetta Stone for musical Playlists.
# TODO
- [ ] Build connectors to services
	- [ ] Youtube
	- [ ] Spotify
- [ ] Create list logic with metadata (in memory or write to file)
- [ ] First version is going to be terminal only no need to authentication and it maybe ephimeral meaning the playlist changed won't be stored, it will only swap or appear in a different platform
- [ ] Authentication service (not necesary)
- [ ] Database management
- [ ] GUI