require "option_parser"
require "colorize"

the_beatles = [
  "John Lennon",
  "Paul McCartney",
  "George Harrison",
  "Ringo Starr",
]

say_hi_to = ""
OptionParser.parse do |parser|
  parser.banner = "These are the options for playper-cli: "
  #parser.banner = "#{"The Beatles"App"

  parser.on "-v", "--version", "Show version" do
    puts "version 0.1.0"
    exit
  end
  parser.on "-h", "--help", "Show help" do
    puts parser
    exit
  end
  parser.on "-sp", "--spotify", "Show version" do
    puts "Make use of spotify info."
    exit
  end
  parser.on "-g NAME", "--goodbye_hello=NAME", "Say hello to whoever you want" do |name|
    say_hi_to = name
  end
  # Exception Handling
  parser.missing_option do |option_flag|
    STDERR.puts "ERROR: #{option_flag} is missing something.".colorize(:green).on(:black)
    STDERR.puts ""
    STDERR.puts parser
    exit(1)
  end
  parser.invalid_option do |option_flag|
    STDERR.puts "ERROR: #{option_flag} is not a valid option.".colorize(:yellow).on(:black)
    STDERR.puts parser
    exit(1)
  end
  #puts parser
end

unless say_hi_to.empty?
  puts ""
  puts "You say goodbye, and #{the_beatles.sample} says hello to #{say_hi_to}!"
end
