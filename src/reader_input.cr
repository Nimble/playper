require "colorize"
require "./hosts/*"


# This is a bit of a metaclass for now
# Its going to be abstract class for input also working with cases
# Middleware between each host and html template
# Directware if used as CLI tool
  # Should read

module Reader_Input
  def cli_input
      #switch()
    puts "Welcome to The Beatles Sing-Along version 1.0!"
    puts "Enter a phrase you want The Beatles to sing"
    print "> "
    user_input = gets

    exit if user_input.nil? # Ctrl+D

    default_lyrics = "Na, na, na, na-na-na na" \
                    " / " \
                    "Na-na-na na, hey Jude"

    lyrics = user_input.presence || default_lyrics

    #puts "The Beatles are singing: #{"🎵#{lyrics}🎶🎸🥁".colorize.mode(:blink)}"
  end

end
