require "xml"
# Interacting with XSPF playlist files used by clementine
# XSPF
# Input
# swapper.cr
# Output

module API_XSPF
    VERSION = "0.1.0"

    # TODO: Put your code here
#<?xml version="1.0" encoding="UTF-8"?>
#  <playlist version="1" xmlns="http://xspf.org/ns/0/">
#    <trackList>
#      <track>

#        <location>/media/Plex/Music/11. KOMM,SUSSER TOD(M-10 DIRECTOR'S EDIT.VERSION).flac</location>
#        <title>11. KOMM,SUSSER TOD(M-10 DIRECTOR'S EDIT.VERSION)</title>
#        <duration>463000</duration>
#        <image>/media/Plex/Music/692b6cb2e35e18c27b32e0632a07dd47.sync-conflict-20220110-215510-IRXNWES.png</image>
#      </track>
#  </playlist>

#Read FROM
file = File.new("Playlist5.xspf")
content = file.gets_to_end
filez = "Playlist5.xspf"
#puts content
xml_file = XML::Reader.new(content)
file.close
  #def self.new(io : IO, options : XML::ParserOptions = XML::ParserOptions.default) end

  #files = File.read("path/to/doc.xml")
  #xml = XML.parse(files)
  #node = xml.xpath_node("//foo/@ID")
  #node.text

  reader = Char::Reader.new("aé")
  puts "#{typeof(xml_file)}  <- Tipo de XML"
  #puts "IO READTEST #{xml_file.read}"
  puts "IO TEST #{xml_file.inspect}"
  #puts "IO READ INNER #{xml_file.read_inner_xml}"
  puts "IO DEPTH #{xml_file.depth}"
  puts "IO READ OUTER #{xml_file.read_outer_xml}"
  #xml_file = XML.Reader.new(io : IO, options : XML::ParserOptions = XML::ParserOptions.default)
  #def self.parse(io : IO, options : ParserOptions = ParserOptions.default) : Node
  #end

  xml = <<-XML
  <person id="id-1">
    <firstname>Jane</firstname>
    <lastname>Doe</lastname>
  </person>
  <person id="id-2">
    <firstname>Albert</firstname>
    <lastname>Doe</lastname>
  </person>
  XML

  document = XML.parse(xml)             # : XML::Node
  person = document.first_element_child # : XML::Node?
  persons = document.children           # XML::NodeSet
  if person
    puts person["id"] # "1" : String?

    puts typeof(person.children)                       # XML::NodeSet
    person.children.select(&.element?).each do |child| # Select only element children
    #document.children.select(&.element?).each do |child| # Select only element children
      #puts child["id"]
      puts typeof(child)                               # XML::Node
      puts child.name                                  # firstname : String
      puts child.content                               # Jane : String?
    end
    persona = person.next
    puts "#{typeof(person)}  <- Tipo de person"
    puts "#{typeof(persona)}  <- Tipo de persona "
    puts "#{typeof(persons)}  <- Tipo de persons"
    #puts persona["id"]
  end

#Write TO
  string = XML.build(indent: "  ") do |xml|
    xml.element("person", id: 3) do
      xml.element("firstname") { xml.text "Jean" }
      xml.element("lastname") { xml.text "D'Arc" }
    end
  end



end
