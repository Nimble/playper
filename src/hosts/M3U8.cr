# M3U8
# Input
# swapper.cr
# Output
require "m3u8"

module Local_M3U8
  include M3U8
  # Read Playlist
  file = File.read "spec/playlists/master.m3u8"
  playlist = Playlist.parse(file)
  playlist.master? # => true

  # Generate playlist/stream
  playlist = Playlist.new
  playlist.items << SegmentItem.new(duration: 10.991, segment: "test_01.ts")
  playlist.to_s
end
